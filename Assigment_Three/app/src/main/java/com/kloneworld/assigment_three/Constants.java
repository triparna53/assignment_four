package com.kloneworld.assigment_three;

/**
 * Created by kloneworld on 06/10/17.
 */

public class Constants {

    public static final String BaseURL = "http://autogram-server.appspot.com";
    public static final String GetDetails = "/candidate/get-details";
    public static final String Token = "token";
    public static final String Service = "/serve";
    public static final String Key = "key";
    public static final String KeyType= "ktype";
    public static final String KeyTypeVideo ="video";
    public static final String KeyTypeAudio ="audio";
}
